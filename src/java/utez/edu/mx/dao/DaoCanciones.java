package utez.edu.mx.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import utez.edu.mx.bean.BeanCanciones;
import utils.ConexionMySQL;

public class DaoCanciones {

    Connection conexion;
    PreparedStatement ps;
    ResultSet rs;

    private final String sqlConsultar = "SELECT * FROM CANCION;";

    public List<BeanCanciones> consultarCanciones() {
        List<BeanCanciones> entrenadors = new ArrayList<>();

        try {
            conexion = ConexionMySQL.getConnection();
            ps = conexion.prepareStatement(sqlConsultar);
            rs = ps.executeQuery();

            while (rs.next()) {
                BeanCanciones entrenador = new BeanCanciones(rs.getInt("idCancion"), rs.getString("nombre"), rs.getString("artista"), rs.getString("duracion"), rs.getString("genero"));
                entrenadors.add(entrenador);
            }

            System.out.println("-> " + entrenadors);
        } catch (Exception e) {
            System.err.println("Error -> " + e.getMessage());
        } finally {
            cerrar();
        }
        return entrenadors;
    }

      public BeanCanciones consultaIndividual(int idCanciones) {
        BeanCanciones cancionConsultada = new BeanCanciones();
        try {
            conexion = ConexionMySQL.getConnection();
            ps = conexion.prepareStatement("SELECT * FROM CANCION WHERE idCancion = ?;");
            ps.setInt(1, idCanciones);
            rs = ps.executeQuery();
            while (rs.next()) {
                cancionConsultada.setIdCancion(rs.getInt("idCancion"));
                cancionConsultada.setNombre(rs.getString("nombre"));
                cancionConsultada.setArtista((rs.getString("artista")));
                cancionConsultada.setDuracion(rs.getString("duracion"));
                cancionConsultada.setGenero(rs.getString("genero"));
            }
        } catch (SQLException ex) {
            System.out.println("Error DaoCanciones- consultaIndividual()" + ex);
        } finally {
            cerrar();
        }
        return cancionConsultada;
    }
      
    private final String sqlRegistrar = "INSERT INTO CANCION(nombre, artista, duracion, genero) VALUES (?,?,?,?);";

    public int registrarCanciones(BeanCanciones unaCancion) {
        int idNuevo = 0;
        try {
            conexion = ConexionMySQL.getConnection();
            ps = conexion.prepareStatement(sqlRegistrar, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, unaCancion.getNombre());
            ps.setString(2, unaCancion.getArtista());
            ps.setString(3, unaCancion.getDuracion());
            ps.setString(4, unaCancion.getGenero());
            if (ps.executeUpdate() == 1) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    idNuevo = rs.getInt(1);
                }
            }
        } catch (SQLException e) {
            System.err.println("Error registrarCanciones()" + e.getMessage());
        } finally {
            cerrar();
        }
        return idNuevo;
    }

    private final String sqlModificar = "UPDATE CANCION SET nombre = ?, artista = ?, duracion = ?, genero = ? WHERE idCancion = ?;";

    public boolean modificarCancion(BeanCanciones unaCancion) {
        boolean modificado = false;
        try {
            conexion = ConexionMySQL.getConnection();
            ps = conexion.prepareStatement(sqlModificar);
            ps.setString(1, unaCancion.getNombre());
            ps.setString(2, unaCancion.getArtista());
            ps.setString(3, unaCancion.getDuracion());
            ps.setString(4, unaCancion.getGenero());
            ps.setInt(5, unaCancion.getIdCancion());
            modificado = ps.executeUpdate() == 1;
            System.out.println("Modificado");
            System.out.println(modificado);
        } catch (SQLException e) {
            System.err.println("Error en el método sql -> " + e.getMessage());
        } finally {
            cerrar();
        }
        return modificado;
    }

    private final String sqlEliminar = "DELETE FROM CANCION WHERE idCancion = ?;";

    public boolean eliminarCancion(int idCancion) {
        boolean eliminado = false;
        try {
            conexion = ConexionMySQL.getConnection();
            ps = conexion.prepareStatement(sqlEliminar);
            ps.setInt(1, idCancion);

            eliminado = ps.executeUpdate() == 1;

        } catch (SQLException e) {
            System.err.println("Error en el método sql -> " + e.getMessage());
        } finally {
        }
        return eliminado;
    }

    public void cerrar() {
        try {
            if (conexion != null) {
                conexion.close();
            }
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        } catch (Exception e) {
            System.err.println("Error en cerrar conexión" + e.getMessage());
        }
    }
}
