package utez.edu.mx.control;

import com.google.gson.Gson;
import com.opensymphony.xwork2.ActionSupport;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import org.jboss.logging.Logger;
import utez.edu.mx.bean.BeanCanciones;
import utez.edu.mx.dao.DaoCanciones;

public class ControlCanciones extends ActionSupport {

    //AJAX------------------------
    private String parametro; // Para recibir los datos.
    private Map respuestas;
    int id;
    //STRUTS ----------------------
    BeanCanciones bean;
    DaoCanciones dao;
    List<BeanCanciones> lista;
    String mensaje;

    // CONTROL ANGULA ----------------------
    public String registrarCancionJson() {
        bean = new Gson().fromJson(parametro, BeanCanciones.class);
        dao = new DaoCanciones();
        System.out.println("---> " + parametro);
        if (dao.registrarCanciones(bean) > 1) {
            mensaje = "Cancion Registrada con Exito";
        } else {
            mensaje = "Ocurrio un error, intenta nuevamente";
        }
        return SUCCESS;
    }

    public String modificarCancionJson() {
        dao = new DaoCanciones();
        bean = new Gson().fromJson(parametro, BeanCanciones.class);
        if (dao.modificarCancion(bean)) {
            mensaje = "Cancion Modificada con Exito";
        } else {
            mensaje = "Ocurrio un error, intenta nuevamente";
        }
        return SUCCESS;
    }

    public String eliminarCancionJson() {
        dao = new DaoCanciones();
        if (dao.eliminarCancion(id)) {
            mensaje = "Cancion Eliminada de la Lista";
            return SUCCESS;
        } else {
            mensaje = "No se Elimino algún Registro";
            return ERROR;
        }
    }

    // CONTROL STRUTS --------------
    public String inicioCanciones() {
        dao = new DaoCanciones();
        lista = dao.consultarCanciones();
        return SUCCESS;
    }

    public String consultaIndividual() {
        dao = new DaoCanciones();
        try {
            bean = dao.consultaIndividual(bean.getIdCancion());
            return SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error ControlCanciones - consultaIndividual()" + e.getMessage());
            return ERROR;
        }
    }

    public String registrarCanciones() {
        dao = new DaoCanciones();
        try {
            dao.registrarCanciones(bean);
            lista = dao.consultarCanciones();
            System.out.println("Bean Canción Registrada --> " + bean);
            return SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error ControlCanciones - registrarCanciones()" + e.getMessage());
            return ERROR;
        }
    }

    public String consultaIndividual1() {
        dao = new DaoCanciones();
        try {
            System.out.println("BEAN --->" + bean.getIdCancion());
            bean = dao.consultaIndividual(bean.getIdCancion());
            System.out.println("Consulta Individual - Control " + bean.getIdCancion());
            return SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error ControlCanciones - consultaIndividual()" + e.getMessage());
            return ERROR;
        }

    }

    public String modificarCanciones() {
        dao = new DaoCanciones();
        try {
            dao.modificarCancion(bean);
            lista = dao.consultarCanciones();
            return SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error ControlCaciones - modificarCanciones()" + e.getMessage());
            return ERROR;
        }
    }

    public String eliminarCanciones() {
        dao = new DaoCanciones();
        try {
            dao.eliminarCancion(bean.getIdCancion());
            lista = dao.consultarCanciones();
            return SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error ControlCanciones - eliminarCanciones()" + e.getMessage());
            return ERROR;
        }
    }

    public BeanCanciones getBean() {
        return bean;
    }

    public void setBean(BeanCanciones bean) {
        this.bean = bean;
    }

    public DaoCanciones getDao() {
        return dao;
    }

    public void setDao(DaoCanciones dao) {
        this.dao = dao;
    }

    public List<BeanCanciones> getLista() {
        return lista;
    }

    public void setLista(List<BeanCanciones> lista) {
        this.lista = lista;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getParametro() {
        return parametro;
    }

    public void setParametro(String parametro) {
        this.parametro = parametro;
    }

    public Map getRespuestas() {
        return respuestas;
    }

    public void setRespuestas(Map respuestas) {
        this.respuestas = respuestas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
