package utez.edu.mx.control;

import static com.opensymphony.xwork2.Action.SUCCESS;
import java.util.HashMap;
import java.util.Map;
import utez.edu.mx.bean.BeanCanciones;
import utez.edu.mx.dao.DaoCanciones;

public class ActionCanciones {

    private String parametro;
    private Map respuestas = new HashMap();
    private DaoCanciones daoCanciones;
    BeanCanciones unaCancion;

    public String consultaGeneral() {
        daoCanciones = new DaoCanciones();

        respuestas.put("objeto", daoCanciones.consultarCanciones());
        return SUCCESS;
    }

   /* public String registrar() {
        unaCancion = new BeanCanciones();
        daoCanciones = new DaoCanciones();

        try {
            JSONObject jsonObj = new JSONObject(parametro);
            unaCancion.setIdProfesor(jsonObj.getString("nombreProfesor"));

        } catch (Exception e) {
            System.out.println("Error Accion registrar");
        }
        int id = daoCanciones.registrarCanciones(unaCancion);
        if (id > 0) {
            unaCancion.setIdCancion(id);
            respuestas.put("mensaje", "Canción registrada exitosamente.");
        }
        respuestas.put("cancionNueva", unaCancion);
        return SUCCESS;
    }*/

    public String getParametro() {
        return parametro;
    }

    public void setParametro(String parametro) {
        this.parametro = parametro;
    }

    public Map getRespuestas() {
        return respuestas;
    }

    public void setRespuestas(Map respuestas) {
        this.respuestas = respuestas;
    }

}
