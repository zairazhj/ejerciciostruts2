package utez.edu.mx.bean;

public class BeanCanciones {

    private int idCancion;
    private String nombre;
    private String artista;
    private String duracion;
    private String genero;

    public BeanCanciones() {
    }

    public BeanCanciones(int idCancion, String nombre, String artista, String duracion, String genero) {
        this.idCancion = idCancion;
        this.nombre = nombre;
        this.artista = artista;
        this.duracion = duracion;
        this.genero = genero;
    }

    public int getIdCancion() {
        return idCancion;
    }

    public void setIdCancion(int idCancion) {
        this.idCancion = idCancion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getArtista() {
        return artista;
    }

    public void setArtista(String artista) {
        this.artista = artista;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
    
        
    
}
