<%-- 
    Document   : index
    Created on : 17/05/2019, 10:34:38 AM
    Author     : CDS
--%>

<%@ taglib prefix="s" uri="/struts-tags" %>
<%String context = request.getContextPath();%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html ng-app="inicioCanciones">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.32/angular.min.js"></script>
        <title>JSP Page</title>
    </head>
    <body ng-controller="controlador1" ng-init="inicio()">
        <div class="container">
            <div class=" jumbotron col-md-12">
                <h1>Lista Canciones</h1>
            </div>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Canciones</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Registro</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Modificar</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab"> <table class="table table-striped table-dark">
                        <thead>
                            <tr style="color: #FACC2E">
                                <th>No.</th>
                                <th>Nombre</th>
                                <th>Artista</th>
                                <th>Duración</th>
                                <th>Genero</th>
                                <th>Modificar</th>
                                <th>Eliminar</th>
                            </tr>
                        </thead>
                        <tbody>

                            <tr  ng-repeat ="cancion in listaCanciones" style="color: white">
                                <td/></td>
                                <td>{{cancion.nombre}}</td>
                                <td>{{cancion.artista}}</td>
                                <td>{{cancion.duracion}}</td>
                                <td>{{cancion.genero}}</td>
                                <td>    

                                    <button class="btn btn-outline-warning" ng-click="setModificar(cancion)"> Modificar </button>
                                </td> 
                                <td>    
                                    <button class="btn btn-outline-danger" ng-click="eliminarCancion(cancion.idCancion)" > Eliminar </button>
                                </td>   
                            </tr>
                        </tbody>

                    </table></div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <fieldset>
                        <legend>Registrar Canciones!</legend>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <s:label>Nombre: </s:label><s:textfield ng-model="bean.nombre" name="bean.nombre"/>
                                    </div>
                                    <div class="form-group">
                                    <s:label>Artista: </s:label><s:textfield ng-model="bean.artista" name="bean.artista"/>
                                    </div> 
                                    <div class="form-group">
                                    <s:label>Duración: </s:label><s:textfield ng-model="bean.duracion" name="bean.duracion"/>
                                    </div>
                                    <div class="form-group">
                                    <s:label>Genero: </s:label><s:select list="{'Banda', 'Rock', 'Reggaeton', 'Pop', 'Hip-Hop/Rap', 'Bachata', 'Metal', 'Duranguense', 'Cumbia','Reggae'}" ng-model="bean.genero" name="bean.genero"></s:select>
                                    </div>
                                    <button class="btn btn-success" ng-click="registrarCanciones()">Enviar </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                        <fieldset>
                            <legend>Modificar Canciones!</legend>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                    <s:label>Nombre: </s:label><s:textfield ng-model="beanM.nombre" name="bean.nombre"/>
                                    </div>
                                    <div class="form-group">
                                    <s:label>Artista: </s:label><s:textfield ng-model="beanM.artista" name="bean.artista"/>
                                    </div> 
                                    <div class="form-group">
                                    <s:label>Duración: </s:label><s:textfield ng-model="beanM.duracion" name="bean.duracion"/>
                                    </div>
                                    <div class="form-group">
                                    <s:label>Genero: </s:label><s:select list="{'Banda', 'Rock', 'Reggaeton', 'Pop', 'Hip-Hop/Rap', 'Bachata', 'Metal', 'Duranguense', 'Cumbia','Reggae'}"></s:select>
                                    </div>
                                    <button class="btn btn-success" ng-click="modificarCancionesJson()">Enviar </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </body>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" ></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
            <script src="<%=context%>/js/acciones.js"></script>

</html>

