<%-- 
    Document   : registrarCanciones
    Created on : 20/05/2019, 08:59:00 AM
    Author     : CDS
--%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<%String context = request.getContextPath();%>
<!DOCTYPE html>
<html ng-app="inicioCanciones">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body ng-controller="controlador1">
    <center>
        <fieldset>
            <legend>Registrar Canciones!</legend>
            {{bean}}
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <s:label>Nombre: </s:label><s:textfield ng-model="bean.nombre" name="bean.nombre"/>
                        </div>
                        <div class="form-group">
                            <s:label>Artista: </s:label><s:textfield ng-model="bean.artista" name="bean.artista"/>
                        </div> 
                        <div class="form-group">
                            <s:label>Duración: </s:label><s:textfield ng-model="bean.duracion" name="bean.duracion"/>
                        </div>
                        <div class="form-group">
                            <s:label>Genero: </s:label><s:select list="{'Banda', 'Rock', 'Reggaeton', 'Pop', 'Hip-Hop/Rap', 'Bachata'}" label="genero" ng-model="bean.genero" name="bean.genero"></s:select>
                        </div>
                        <button class="btn btn-success" ng-click="registrarCanciones()">Enviar </button>
                    </div>
                </div>
        </fieldset>
    </center>
</body>
 <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" ></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="<%=context%>/js/acciones.js"></script>
</html>
