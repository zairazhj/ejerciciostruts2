<%-- 
    Document   : modificarCanciones
    Created on : 20/05/2019, 08:25:29 AM
    Author     : CDS
--%>

<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<%String context = request.getContextPath();%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body style="background-color:  #d6eaf8">
    <center>
        <fieldset>
            <div class=" jumbotron col-md-12">
                <h1>Modificar Canciones</h1>
            </div>
            <s:form theme="simple" action="modificarCanciones">
                <div class="row">
                    <div class="col">
                        <s:hidden name="bean.idCancion" value="%{bean.idCancion}"/>
                        <div class="col-md-4 offset-4">
                            <s:label>Nombre: </s:label><s:textfield class="form-control"  name="bean.nombre" value="%{bean.nombre}"/>
                            </div>
                            <div class="col-md-4 offset-4">
                            <s:label>Artista: </s:label><s:textfield class="form-control" name="bean.artista" value="%{bean.artista}"/>
                            </div> 
                            <div class="col-md-4 offset-4">
                            <s:label>Duración: </s:label><s:textfield class="form-control" name="bean.duracion" value="%{bean.duracion}"/>
                            </div>
                            <div class="col-md-4 offset-4">
                            <s:label>Genero: </s:label><s:select class="form-control" list="{'Banda', 'Rock', 'Reggaeton', 'Pop', 'Hip-Hop/Rap', 'Bachata'}" label="genero" name="bean.genero" value="%{bean.genero}"></s:select>
                            </div>
                        <s:submit class="btn btn-success" value="Enviar"/>
                        <s:form action="inicioCanciones">
                            <button class="btn btn-outline-danger" type="submit" > Regresar </button>
                        </s:form>
                    </div>
                </div>
            </s:form>
        </fieldset>
    </center>

</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</html>
