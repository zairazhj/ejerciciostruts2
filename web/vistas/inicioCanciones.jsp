<%-- 
    Document   : inicioCanciones
    Created on : 17/05/2019, 08:37:15 AM
    Author     : CDS
--%>

<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%String context = request.getContextPath();%>
<!DOCTYPE html>
<html lang="en" ng-app="inicioCanciones">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Canciones</title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    </head>
    <body ng-controller="controlador1" ng-init="inicio()">
        <div class="container">
            <br>
            <center><h1>Lista de Canciones!</h1></center>
            <br>
            {{mensaje}}
            <a class="btn btn-success" href="<%=context%>/vistas/registrarCanciones.jsp">Registrar</a>
            <br>
            <br>
            <div class="row">
                <table class="table table-striped table-dark">
                    <thead>
                        <tr style="color: #FACC2E">
                            <th>No.</th>
                            <th>Nombre</th>
                            <th>Artista</th>
                            <th>Duración</th>
                            <th>Genero</th>
                            <th>Modificar</th>
                            <th>Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                            <tr  ng-repeat ="cancion in listaCanciones" style="color: white">
                                <td/></td>
                                <td>{{cancion.nombre}}</td>
                                <td>{{cancion.artista}}</td>
                                <td>{{cancion.duracion}}</td>
                                <td>{{cancion.genero}}</td>
                                <td>    
                                    <s:form action="consultaIndividual">
                                        <s:hidden name="bean.idCancion" value="{{cancion.idCancion}}" />
                                        <button class="btn btn-outline-warning" type="submit" > Modificar </button>
                                    </s:form>
                                </td> 
                                <td>    
                                    <s:form action="eliminarCanciones">
                                        <s:hidden name="bean.idCancion" value="%{idCancion}" />
                                        <button class="btn btn-outline-danger"type="submit" > Eliminar </button>
                                    </s:form>
                                </td>   
                            </tr>

                    </tbody>

                </table>

            </div>
        </div>
    </body>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" ></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="<%=context%>/js/acciones.js"></script>

</html>
