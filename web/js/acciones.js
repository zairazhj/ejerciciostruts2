var raiz = window.location.origin + '/Ejercicio/';

angular.module('inicioCanciones', []).controller('controlador1', function ($scope, $http) {
    $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    $scope.mensaje;
    $scope.listaCanciones = [];
    $scope.bean = {};
    $scope.beanM = {};

    $scope.inicio = function () {
        $http.post(raiz + 'inicioCancionesJson', {}).then(function (response) {
            $scope.listaCanciones = response.data.lista;
        }, function (response) {
        });
    }

    $scope.registrarCanciones = function () {
        $http.post(raiz + 'registrarCancionJson', "parametro=" + angular.toJson($scope.bean)).then(function (response) {
            $scope.mensaje = response.data.mensaje;
            $('#home-tab').click();
            alert($scope.mensaje);
            $scope.inicio();
        }, function (response) {
        });
    }

    $scope.setModificar = function (cancion) {
        angular.copy(cancion, $scope.beanM);
        $('#contact-tab').click();
    }

    $scope.modificarCancionesJson = function () {
        $http.post(raiz + 'modificarCancionJson', "parametro=" + angular.toJson($scope.beanM)).then(function (response) {
            $scope.mensaje = response.data.mensaje;
            $('#home-tab').click();
            alert($scope.mensaje);
            $scope.inicio();
        }, function (response) {
        });
    }

    $scope.eliminarCancion = function (id) {
        $http.post(raiz + 'eliminarCancionJson', "id=" + id).then(function (response) {
            $scope.mensaje = response.data.mensaje;
            alert($scope.mensaje);
            $scope.inicio();
        }, function (response) {
        });
    }
});